package ru.strongit.database.dao

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

import java.util.ArrayList
import java.util.Collections
import java.util.Comparator
import ru.strongit.database.entity.Fillup

class FillupDao {


    private val fillups: MutableList<Fillup>

    private var fillupListener: ValueEventListener? = null


    interface UpdateListener {
        fun fillupUpdated()
    }

    init {
        fillups = ArrayList()
    }

    fun beginFillupsObservation(notification: UpdateListener) {
        val user = FirebaseAuth.getInstance().currentUser
        val ref = database.getReference(user!!.uid).child(KEY_FILLUPS)

        fillupListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                fillups.clear()
                //                for (DataSnapshot item : dataSnapshot.getChildren()) {
                //                    Fillup fillup = new Fillup();
                //
                //                    fillup.setId(item.getKey());
                //                    fillup.setName(item.child("name").getValue(String.class));
                //                    fillup.setCooldown(item.child("cooldown").getValue(Integer.class));
                //
                //                    fillups.add(fillup);
                //                }

                Collections.sort(fillups) { o1, o2 -> o1.getDate().compareTo(o2.getDate()) }

                notification.fillupUpdated()
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        }

        ref.addValueEventListener(fillupListener!!)
    }

    fun removeFillupListener() {
        val user = FirebaseAuth.getInstance().currentUser
        if (fillupListener != null) {
            FirebaseDatabase.getInstance().getReference(user!!.uid).child(KEY_FILLUPS).removeEventListener(fillupListener!!)
        }
    }

    fun addFillup(fillup: Fillup) {
        val user = FirebaseAuth.getInstance().currentUser
        val key = database.getReference(user!!.uid)
                .child(KEY_FILLUPS)
                .push()
                .key
        fillup.setKey(key)

        val ref = database.getReference("users/" + user.uid)
                .child(KEY_FILLUPS)
                .child(key!!)
        ref.setValue(fillup)
    }

    fun deleteFillup(key: String) {
        val user = FirebaseAuth.getInstance().currentUser
        val ref = database.getReference(user!!.uid)
                .child(KEY_FILLUPS)
                .child(key)
        ref.removeValue()

    }

    fun getFillups(): List<Fillup> {
        return fillups
    }

    fun fillupIndex(key: String): Int {
        var i = 0
        while (i < fillups.size) {
            if (fillups[i].getKey() == key)
                return i
            i++
        }
        return -1
    }

    companion object {

        private val KEY_FILLUPS = "fillups"

        private val database = FirebaseDatabase.getInstance()
        private var dao: FillupDao? = null

        val instance: FillupDao
            get() {
                if (dao == null) {
                    dao = FillupDao()
                    database.setPersistenceEnabled(true)
                }
                return dao!!
            }
    }

}