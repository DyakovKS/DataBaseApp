package ru.strongit.database.entity;

import java.util.Date;
import java.util.UUID;

public class Fillup {
    public Integer id;
    public String key;
    public Date date;
    public Float volume;
    public Float price;
    public Float summa;
    public Float odometr;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Float getVolume() {
        return volume;
    }

    public void setVolume(Float volume) {
        this.volume = volume;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getSumma() {
        return summa;
    }

    public void setSumma(Float summa) {
        this.summa = summa;
    }

    public Float getOdometr() {
        return odometr;
    }

    public void setOdometr(Float odometr) {
        this.odometr = odometr;
    }
}
