package ru.strongit.database;

import android.app.Service;

import com.google.firebase.messaging.FirebaseMessagingService;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        String refreshedToken = s;
        android.util.Log.d("TAGX", "Refreshed token: " + refreshedToken);
    }
}
