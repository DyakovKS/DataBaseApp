package ru.strongit.database

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button


import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_main.*

import java.util.ArrayList
import java.util.Arrays
import java.util.Date

import ru.strongit.database.dao.FillupDao
import ru.strongit.database.entity.Fillup


class MainActivity : AppCompatActivity() {
    private var btnLogout: Button? = null
    private val TAG = "TAGX"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initUI()
    }

    private fun initUI() {
        btnLogout = findViewById(R.id.btnLogOut)
        btnLogout!!.setOnClickListener {
            logout()

        }
        btn_add_fillup.setOnClickListener { db3() }

        //btn_add2.setOnClickListener()
    }

    private fun logout() {

    }

    override fun onStart() {
        super.onStart()

        //db();

        auth()

        //        db2();
        //
        //        db3();
        //
        //        db4();
    }

    private fun db3() {

        val fillup = Fillup()
        fillup.date = Date(System.currentTimeMillis())
        fillup.id = 113
        fillup.odometr = 4234f
        fillup.price = 50f
        fillup.summa = 234f


        val fillupDao = FillupDao()
        fillupDao.addFillup(fillup)
    }

    private fun db2() {
        val database = FirebaseDatabase.getInstance()
        val ref = database.getReference("users/" + FirebaseAuth.getInstance().uid!!)

        val myRef = ref.child("fillups")

        val fillup = Fillup()
        fillup.date = Date()
        fillup.odometr = java.lang.Float.valueOf(250000f)
        fillup.price = 45.67f
        fillup.summa = 456.7f
        fillup.volume = 10f

        val fillups = ArrayList<Fillup>()
        fillups.add(fillup)
        fillups.add(fillup)

        myRef.setValue(fillups)

    }

    private fun db() {
        val database = FirebaseDatabase.getInstance()

        val myRef = database.getReference("message1")


        // Read from the database
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                val value = dataSnapshot.getValue(String::class.java)
                Log.d(TAG, "Value is: " + value!!)
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException())
            }
        })

        myRef.setValue("Hello, World!")
    }


    private fun db4() {
        val fillupDao = FillupDao()
//        val filups = fillupDao.fillups
//
//        Log.d(TAG, "db4: " + filups.toString())
    }

    private fun auth() {

        // Choose authentication providers
        val providers = Arrays.asList<AuthUI.IdpConfig>(
                AuthUI.IdpConfig.EmailBuilder().build()
                , AuthUI.IdpConfig.PhoneBuilder().build()
                , AuthUI.IdpConfig.GoogleBuilder().build()
                //,new AuthUI.IdpConfig.TwitterBuilder().build()
                //,new AuthUI.IdpConfig.FacebookBuilder().build()
        )

        // Create and launch sign-in intent
        startActivityForResult(
                AuthUI.getInstance().createSignInIntentBuilder()
                        .setAvailableProviders(providers).build(), RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val bundle = data!!.extras
        Log.d(TAG, "onActivityResult: " + bundle!!.toString())

        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {
                // Successfully signed in
                val user = FirebaseAuth.getInstance().currentUser
                Log.d(TAG, "onActivityResult: " + user!!.toString())
                // ...
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
            }
        }
    }

    companion object {
        private val RC_SIGN_IN = 123
    }
}
